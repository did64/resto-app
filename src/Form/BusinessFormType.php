<?php

namespace App\Form;

use App\Entity\Business;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BusinessFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class,[
                'label' => 'url d\'accès au site'
            ])
            ->add('commercial_name',TextType::class,[
                'label' => 'nom'
            ])
            ->add('address', TextType::class,[
                'label' => 'adresse'
            ])
            ->add('phone', TelType::class,[
                'label' => 'téléphone fixe',
                'required' => false
            ])
            ->add('email', EmailType::class, [
                'label' => 'email',
                'required' => false
            ])
            ->add('cp', TextType::class,[
                'label' => 'code postal'
            ])
            ->add('city', TextType::class,[
                'label' => 'commune'
            ])
            ->add('mobile', TelType::class,[
                'label' => 'téléphone mobile',
                'required' => false
            ])
            ->add('facebook', UrlType::class,[
                'label' => 'Compte Facebook',
                'required' => false
            ])
            ->add('twitter',UrlType::class,[
                'label' => 'Compte Twitter',
                'required' => false
            ])
            ->add('instagram',UrlType::class,[
                'label' => 'Compte Instagram',
                'required' => false
            ])
            ->add('valider', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Business::class,
        ]);
    }
}
