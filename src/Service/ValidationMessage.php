<?php


namespace App\Service;


class ValidationMessage
{
    //Class contenant les messages de validation de formulaire


    private $state = true;// true ou false si formulaire valide
    private $message = ""; // si state false alors message d'erreur sinon message de réussite
    private $attachment; //attaché un message à un element du dom, une class css

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state): void
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getAttachment()
    {
        return $this->attachment;
    }

    /**
     * @param mixed $attachment
     */
    public function setAttachment($attachment): void
    {
        $this->attachment = $attachment;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message): void
    {
        $this->message = $message;
    }

    public function __toString(){
        return $this->message;
    }


}