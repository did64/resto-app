<?php

namespace App\Repository;

use App\Entity\OnBoarding;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method OnBoarding|null find($id, $lockMode = null, $lockVersion = null)
 * @method OnBoarding|null findOneBy(array $criteria, array $orderBy = null)
 * @method OnBoarding[]    findAll()
 * @method OnBoarding[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OnBoardingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OnBoarding::class);
    }

    // /**
    //  * @return OnBoarding[] Returns an array of OnBoarding objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OnBoarding
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
