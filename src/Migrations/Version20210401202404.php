<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210401202404 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE background_image (id INT AUTO_INCREMENT NOT NULL, business_id_id INT NOT NULL, file VARCHAR(255) NOT NULL, is_main TINYINT(1) NOT NULL, INDEX IDX_941A96781A579E8 (business_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE description (id INT AUTO_INCREMENT NOT NULL, business_id_id INT NOT NULL, main_text LONGTEXT NOT NULL, second_text LONGTEXT DEFAULT NULL, start_time VARCHAR(50) NOT NULL, pause VARCHAR(50) DEFAULT NULL, restart_time VARCHAR(50) DEFAULT NULL, end_time VARCHAR(50) NOT NULL, INDEX IDX_6DE440261A579E8 (business_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE nav_link (id INT AUTO_INCREMENT NOT NULL, business_id_id INT NOT NULL, label VARCHAR(100) NOT NULL, INDEX IDX_FD52DA9A1A579E8 (business_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE products (id INT AUTO_INCREMENT NOT NULL, business_id_id INT NOT NULL, title VARCHAR(255) NOT NULL, content VARCHAR(255) NOT NULL, link VARCHAR(255) NOT NULL, INDEX IDX_B3BA5A5A1A579E8 (business_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE background_image ADD CONSTRAINT FK_941A96781A579E8 FOREIGN KEY (business_id_id) REFERENCES business (id)');
        $this->addSql('ALTER TABLE description ADD CONSTRAINT FK_6DE440261A579E8 FOREIGN KEY (business_id_id) REFERENCES business (id)');
        $this->addSql('ALTER TABLE nav_link ADD CONSTRAINT FK_FD52DA9A1A579E8 FOREIGN KEY (business_id_id) REFERENCES business (id)');
        $this->addSql('ALTER TABLE products ADD CONSTRAINT FK_B3BA5A5A1A579E8 FOREIGN KEY (business_id_id) REFERENCES business (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE background_image');
        $this->addSql('DROP TABLE description');
        $this->addSql('DROP TABLE nav_link');
        $this->addSql('DROP TABLE products');
    }
}
