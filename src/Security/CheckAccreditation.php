<?php


namespace App\Security;


use Symfony\Component\HttpFoundation\Session\SessionInterface;





class CheckAccreditation
{
    private $session;

    public function __construct( SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @param $businessName
     * @param $userId
     * @return bool
     * vérifie que l'utilisateur fait partie du business de la session en cours et que le nom du business passé
     * dans l'url est le même que celui en session
     */
    public function isAccredited($businessName, $userId){
        if($businessName != $this->session->get('businessName') || !in_array($userId, $this->session->get('businessUsers'))){
            return false;
        }else{
            return true;
        }
    }

    /**
     * @param $userId
     * @return bool
     * vérifie que l'utilisateur fait partie du business de la session en cours
     */
    public function userIsFromCurrentBusiness($userId){
        if(!in_array($userId, $this->session->get('businessUsers'))){
            return false;
        }else{
            return true;
        }
    }

}