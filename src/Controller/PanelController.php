<?php

namespace App\Controller;

use App\Entity\User;
use App\Security\CheckAccreditation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class PanelController extends AbstractController
{
    private $checkAccreditation;

    public function __construct( CheckAccreditation $checkAccreditation)
    {
        $this->checkAccreditation = $checkAccreditation;
    }

    /**
     * @Route("{name}/compte", name="app_panel")
     * @param $name
     * @param SessionInterface $session
     * @return Response
     */
    public function index($name, SessionInterface $session)
    {
        $idCurrentUser = $session->get('user')->getId();
        if(!$this->checkAccreditation->isAccredited($name, $idCurrentUser)){
            //redirigé vers la page compte du businnes du user en session/ si on tente d'aller sur un autre business que le sien
           return $this->redirectToRoute('app_panel',array('name' =>$session->get('businessName')));
        }

        $db = $this->getDoctrine()->getManager();
        $user = $db->getRepository(User::class)->findOneByIdJoinedToBusiness($idCurrentUser);
        return $this->render('panel/index.html.twig', [
            'businessName' => $user->getBusiness()->getCommercialName(),
        ]);
    }
}
