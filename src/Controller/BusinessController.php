<?php

namespace App\Controller;

use App\Entity\Business;
use App\Entity\OnBoarding;
use App\Entity\User;
use App\Form\BusinessFormType;
use App\Form\RegistrationType;
use App\Security\CheckAccreditation;
use App\Service\ValidationMessage;
use DateTime;
use DateTimeZone;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class BusinessController extends AbstractController
{
    private $checkAccreditation;
    private $session;

    public function __construct(CheckAccreditation $checkAccreditation, SessionInterface $session)
    {
        $this->checkAccreditation = $checkAccreditation;
        $this->session = $session;
    }


    /**
     * @Route("structure/creation/{token}", name="app_panel_create_business")
     * @param Request $request
     * @param ValidationMessage $validationMessage
     * @param UserPasswordEncoderInterface $encoder
     * @param $token
     * @return Response
     */
    public function addBusiness(Request $request, ValidationMessage $validationMessage, UserPasswordEncoderInterface $encoder, $token){
        $idToken = $this->tokenExist($token);
        if(!$idToken){
            $response = new Response();
            $response->headers->set('Content-Type', 'text/html');
            $response->setStatusCode(401);
            return $response;
        }
        $business = new Business();
        $user = new User();
//      $form = $this->createForm(BusinessFormType::class, $business);
//      affichage du formulaire regroupant les deux entités
        $form = $this->createForm(RegistrationType::class, ['user' => $user, 'business' => $business]);

        if($request->isMethod('POST')) {
            $form->handleRequest($request);

            //encodage du mot de passe
            $plainPassword = $form['user']->get('password')->getData();
            $encoded = $encoder->encodePassword($user, $plainPassword);
            $user->setPassword($encoded);
            $user->setRoles(['roles' => 'ROLE_ADMIN']);
            $user->setBusiness($business);
            $business->addUser($user);

            if(!empty($form['business']->get('email')->getData()) && !filter_var($form['business']->get('email')->getData(), FILTER_VALIDATE_EMAIL)){
                $validationMessage->setState(false);
                $validationMessage->setMessage("l'adresse email n'est pas valide");
            }elseif (
                (!empty($form['business']->get('facebook')->getData()) && !filter_var($form['business']->get('facebook')->getData(), FILTER_VALIDATE_URL)) ||
                (!empty($form['business']->get('twitter')->getData()) && !filter_var($form['business']->get('twitter')->getData(), FILTER_VALIDATE_URL)) ||
                (!empty($form['business']->get('twitter')->getData()) && !filter_var($form['business']->get('instagram')->getData(), FILTER_VALIDATE_URL))
            ) {
                $validationMessage->setState(false);
                $validationMessage->setMessage("L'url d'un réseaux social n'est pas valide");
            }elseif(!$this->checkUrl($form['business']->get('name')->getData())){
                $validationMessage->setState(false);
                $validationMessage->setMessage("L'Url ".$form['business']->get('name')->getData()." n'est pas valide");
            }
            if($validationMessage->getState()){
                if($form->isValid()){
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($business);
                    $em->flush();
                    $validationMessage->setMessage("Félicitations, bienvenue sur votre site");
                    $this->deleteToken($idToken);
                    return $this->redirectToRoute('app_panel',array('name' =>$business->getName()));
                    // todo renvoyer vers un formulaire de creation d'un user et le mettre en session avec le business
                }else {
                    $validationMessage->setState(false);
                    $validationMessage->setMessage("Erreur lors de la validation du formulaire, veuillez réessayer.");
                }
            }
        }
        return $this->render("panel/business.html.twig", [
            "form_title" => "Creation structure",
            "form_business" => $form->createView(),
            'validationMessage' => $validationMessage
        ]);
    }


    /**
     * @Route("{name}/compte/informations-structure", name="app_panel_update_business")
     * @param Request $request
     * @param ValidationMessage $validationMessage
     * @param $name
     * @return Response
     */
    public function updateBusiness(Request $request, ValidationMessage $validationMessage, $name){

        $userId = $this->session->get('user')->getId();
        //vérifie que le business est bien celui en session et que l'utilisateur en fait bien parti
        if(!$this->checkAccreditation->isAccredited($name, $userId)){
            return $this->redirectToRoute('app_panel',array('name' =>$this->session->get('businessName')));
        }

        $businessId = $this->session->get('user')->getBusiness()->getId();
        $em = $this->getDoctrine()->getManager();
        $business = $em->getRepository(Business::class)->find($businessId);
        $form = $this->createForm(BusinessFormType::class, $business);


        if($request->isMethod('POST')){
            $form->handleRequest($request);
            if(!empty($form->get('email')->getData()) && !filter_var($form->get('email')->getData(), FILTER_VALIDATE_EMAIL)){
                $validationMessage->setState(false);
                $validationMessage->setMessage("l'adresse email n'est pas valide");
            }elseif (
                (!empty($form->get('facebook')->getData()) && !filter_var($form->get('facebook')->getData(), FILTER_VALIDATE_URL)) ||
                (!empty($form->get('twitter')->getData()) && !filter_var($form->get('twitter')->getData(), FILTER_VALIDATE_URL)) ||
                (!empty($form->get('twitter')->getData()) && !filter_var($form->get('instagram')->getData(), FILTER_VALIDATE_URL))
            )
            {
                $validationMessage->setState(false);
                $validationMessage->setMessage("L'url d'un réseaux social n'est pas valide");
            }elseif(!$this->checkUrl($form->get('name')->getData())){
                $validationMessage->setState(false);
                $validationMessage->setMessage("L'Url ".$form->get('name')->getData()." n'est pas valide");
            }

            if ($validationMessage->getState()) {
                if ($form->isValid()) {
                    $em->flush();
                    $validationMessage->setMessage("Vos informations ont été correctement enregistrées.");
                } else {
                    $validationMessage->setState(false);
                    $validationMessage->setMessage("Erreur lors de la validation du formulaire");
                }
            }
        }

        return $this->render("panel/business.html.twig", [
            "form_title" => "Administration structure",
            "form_business" => $form->createView(),
            'validationMessage' => $validationMessage
        ]);
    }

    public function checkUrl($url){
        return filter_var($url, FILTER_VALIDATE_DOMAIN, FILTER_FLAG_HOSTNAME);
    }

    public function tokenExist($token){
        $db = $this->getDoctrine()->getManager();
        $onBoarding = $db->getRepository(OnBoarding::class)->findOneBy(['token'=>$token]);
        if(empty($onBoarding)){
            return false;
        }
        $now = new Datetime('now', new DateTimeZone('Europe/Paris'));
        $expirationDate = new DateTime($onBoarding->getExpirationDate()->format('Y-m-d H:m:s'), new DateTimeZone('Europe/Paris'));
        if($now < $expirationDate){
            return $onBoarding->getId();
        }
        $db->remove($onBoarding);
        $db->flush();
        return false;
    }

    public function deleteToken($idToken){
        $db = $this->getDoctrine()->getManager();
        $onBoarding = $db->getRepository(OnBoarding::class)->find($idToken);
        $db->remove($onBoarding);
        $db->flush();
    }
}
