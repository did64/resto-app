<?php

namespace App\Controller;

use App\Entity\Business;
use App\Security\CheckAccreditation;
use App\Service\ValidationMessage;
use App\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class UserController extends AbstractController
{
    private $checkAccreditation;
    private $session;

    public function __construct(CheckAccreditation $checkAccreditation, SessionInterface $session)
    {
        $this->checkAccreditation = $checkAccreditation;
        $this->session = $session;
    }

    /**
     * @Route("{name}/compte/gestion-utilisateur", name="app_panel_add_user")
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @param ValidationMessage $validationMessage
     * @param $name
     * @return Response
     */
    public function addUser(Request $request, UserPasswordEncoderInterface $encoder,  ValidationMessage $validationMessage, $name)
    {

        $idCurrentUser = $this->session->get('user')->getId();

        //vérifie que le business est bien celui en session et que l'utilisateur en fait bien partit
        if(!$this->checkAccreditation->isAccredited($name, $idCurrentUser)){
            return $this->redirectToRoute('app_panel',array('name' =>$this->session->get('businessName')));
        }

        // récupère le business du user connecté
        $db = $this->getDoctrine()->getManager();
        $currentUser = $db->getRepository(User::class)->findOneByIdJoinedToBusiness($idCurrentUser);
        $business = $currentUser->getBusiness();
        $businessUsers = $business->getUsers();

        // nouveau user
        $user = new User();

        // construction du form
        $formBuilder = $this->createFormBuilder($user);
        // ajout des champs du user + champ non mappé
        $formBuilder
            ->add('email', EmailType::class)
            ->add('password', PasswordType::class)
            ->add('full_access', CheckboxType::class, [
                'mapped' => false,
                'required' => false,
                'label' => "Donner accès à la modification du contenu du site seulement. L'utilisateur n'aura pas accès à la modification des informations
                personelles ainsi qu'à la création de nouveaux utilisateurs"
            ])
            ->add('save', SubmitType::class);


        $form = $formBuilder->getForm();

        //region réponse formulaire
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            //encodage du mot de passe
            $plainPassword = $request->request->get('form')['password'];
            $encoded = $encoder->encodePassword($user, $plainPassword);
            $user->setPassword($encoded);

            // attribution du business
            $user->setBusiness($business);

            // définition d'un role
            if ($form->get('full_access')->getData()) {
                $user->setRoles(['roles' => 'ROLE_ADMIN']);
            } else {
                $user->setRoles(['roles' => 'ROLE_USER']);
            }

            //validation
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();
                $this->addUserToBusinessUsersInSession($user);
                $validationMessage->setMessage("L'utilisateur a été correctement créé.");
            } else {
                $validationMessage->setState(false);
                $validationMessage->setMessage("Erreur lors de la validation du formulaire, veuillez réessayer.");
            }
        }

        return $this->render('panel/user.html.twig', [
            'form' => $form->createView(),
            'validationMessage' => $validationMessage,
            'businessUsers' => $businessUsers
        ]);
    }


    /**
     * @Route("/{name}/compte/mes-informations", name="app_panel_update_user")
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @param ValidationMessage $validationMessage
     * @param $name
     * @return Response
     */
    public function updateUser(Request $request, UserPasswordEncoderInterface $encoder, ValidationMessage $validationMessage, $name)
    {
        $userId = $this->session->get('user')->getId();

        //vérifie que le business est bien celui en session et que l'utilisateur en fait bien partit
        if(!$this->checkAccreditation->isAccredited($name, $userId)){
            return $this->redirectToRoute('app_panel',array('name' =>$this->session->get('businessName')));
        }

        $db = $this->getDoctrine()->getManager();
        $user = $db->getRepository(User::class)->findOneByIdJoinedToBusiness($userId);

        $formBuilder = $this->createFormBuilder($user);

        $formBuilder
            ->add('email', EmailType::class)
            ->add('old_password', PasswordType::class, [
                'attr' => ['class' => 'js_old_password'],
                'mapped' => false,
                'required' => false,
                'label' => 'Ancien mot de passe'
            ])
            ->add('new_password', PasswordType::class, [
                'mapped' => false,
                'required' => false,
                'label' => 'Nouveau mot de passe'
            ])
            ->add('save', SubmitType::class);

        $form = $formBuilder->getForm();

//        $roles = $user->getRoles();
//        if (in_array('ROLE_ADMIN', $roles)) {
//            $form->get('full_access')->setData(true);
//        }


        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if (!empty($form->get('old_password')->getData())) {
                if ($encoder->isPasswordValid($user, $form->get('old_password')->getData())) {
                    $plainPassword = $form->get('new_password')->getData();
                    $encoded = $encoder->encodePassword($user, $plainPassword);
                    $user->setPassword($encoded);
                } else {
                    $validationMessage->setState(false);
                    $validationMessage->setMessage("Le mot de passe actuel n'est pas valide");
                    $validationMessage->setAttachment('js_old_password');
                }
            }
            if ($validationMessage->getState()) {
//                if ($form->get('full_access')->getData()) {
//                    $user->setRoles(['roles' => 'ROLE_ADMIN']);
//                } else {
//                    $user->setRoles(['roles' => 'ROLE_USER']);
//                }

                if ($form->isValid()) {
                    $db->flush();
                    $this->saveUserInSession($user);
                    $validationMessage->setMessage("Vos informations ont été correctement enregistrées.");
                } else {
                    $db->refresh($user);
                }
            }

        }

        return $this->render('panel/user.html.twig', [
            'form' => $form->createView(),
            'validationMessage' => $validationMessage
        ]);
    }

    /**
     * @Route("/delete-user/{idUser}", name="app_panel_delete_user")
     * @param $idUser
     * @return RedirectResponse
     */
    public function deleteUser($idUser)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        if ($this->checkAccreditation->userIsFromCurrentBusiness($idUser)) {
            $db = $this->getDoctrine()->getManager();
            $userToDelete = $db->getRepository(User::class)->find($idUser);
            $db->remove($userToDelete);
            $db->flush();
            return $this->redirectToRoute('app_panel_add_user',array('name' =>$this->session->get('businessName')));
        }else{
            return $this->redirectToRoute('app_panel',array('name' =>$this->session->get('businessName')));
        }
    }

    /**
     * @Route("/update_role_user/{idUser}/{state}", name="app_panel_update_role_user")
     * @param $idUser
     * @param $state bool
     * modifie le role du user en param en fonction de l'état de la checkbox $state
     * @return RedirectResponse
     */
    public function updateUserRole($idUser, $state)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        if ($this->checkAccreditation->userIsFromCurrentBusiness($idUser)) {
            $db = $this->getDoctrine()->getManager();
            $user = $db->getRepository(User::class)->find($idUser);
            if($state){
                $user->setRoles(['roles' => 'ROLE_ADMIN']);
            }else{
                $user->setRoles(['roles' => 'ROLE_USER']);
            }
            $db->flush();
            return $this->redirectToRoute('app_panel_add_user',array('name' =>$this->session->get('businessName')));
        }else{
            return $this->redirectToRoute('app_panel',array('name' =>$this->session->get('businessName')));
        }
    }

    /**
     * @param
     * met à jour le user en session
     */
    public function saveUserInSession($user){
        $this->session->set('user', $user);
    }

    /**
     * @param $user
     * ajoute l'id du nouvel utilisateur au tableau d'id en session
     */
    public function addUserToBusinessUsersInSession($user){
        $businessUsers = $this->session->get('businessUsers');
        array_push($businessUsers, $user->getId());
        $this->session->set('businessUsers',$businessUsers);
    }
}
