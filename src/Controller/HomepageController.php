<?php

namespace App\Controller;

use App\Entity\Business;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends AbstractController
{
    /**
     * @Route("{name}/home", name="app_home")
     * @param $name
     * @return Response
     */
    public function index($name)
    {
        $db = $this->getDoctrine()->getManager();
        $business = $db->getRepository(Business::class)->findOneBy(['name'=> $name]);
        return $this->render('homepage/index.html.twig', [
            'businessName' => $business->getCommercialName()
        ]);
    }
}
