<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DescriptionRepository")
 */
class Description
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Business", inversedBy="descriptions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $business_id;

    /**
     * @ORM\Column(type="text")
     */
    private $main_text;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $second_text;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $start_time;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $pause;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $restart_time;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $end_time;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBusinessId(): ?Business
    {
        return $this->business_id;
    }

    public function setBusinessId(?Business $business_id): self
    {
        $this->business_id = $business_id;

        return $this;
    }

    public function getMainText(): ?string
    {
        return $this->main_text;
    }

    public function setMainText(string $main_text): self
    {
        $this->main_text = $main_text;

        return $this;
    }

    public function getSecondText(): ?string
    {
        return $this->second_text;
    }

    public function setSecondText(?string $second_text): self
    {
        $this->second_text = $second_text;

        return $this;
    }

    public function getStartTime(): ?string
    {
        return $this->start_time;
    }

    public function setStartTime(string $start_time): self
    {
        $this->start_time = $start_time;

        return $this;
    }

    public function getPause(): ?string
    {
        return $this->pause;
    }

    public function setPause(?string $pause): self
    {
        $this->pause = $pause;

        return $this;
    }

    public function getRestartTime(): ?string
    {
        return $this->restart_time;
    }

    public function setRestartTime(?string $restart_time): self
    {
        $this->restart_time = $restart_time;

        return $this;
    }

    public function getEndTime(): ?string
    {
        return $this->end_time;
    }

    public function setEndTime(string $end_time): self
    {
        $this->end_time = $end_time;

        return $this;
    }
}
