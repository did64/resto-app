<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BusinessRepository")
 * @UniqueEntity("name", message="Cette url est déjà utilisé par une autre structure.")
 */
class Business
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="business", orphanRemoval=true, cascade= {"persist"})
     */
    private $users;

    /**
     * @ORM\Column(type="integer")
     */
    private $cp;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $mobile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $facebook;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $twitter;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $instagram;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $commercial_name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\NavLink", mappedBy="business_id", orphanRemoval=true)
     */
    private $navLinks;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\BackgroundImage", mappedBy="business_id", orphanRemoval=true)
     */
    private $backgroundImages;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Description", mappedBy="business_id", orphanRemoval=true)
     */
    private $descriptions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Products", mappedBy="business_id", orphanRemoval=true)
     */
    private $products;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->navLinks = new ArrayCollection();
        $this->backgroundImages = new ArrayCollection();
        $this->descriptions = new ArrayCollection();
        $this->products = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }
    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setBusiness($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            // set the owning side to null (unless already changed)
            if ($user->getBusiness() === $this) {
                $user->setBusiness(null);
            }
        }

        return $this;
    }

    public function getCp(): ?int
    {
        return $this->cp;
    }

    public function setCp(int $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getMobile(): ?int
    {
        return $this->mobile;
    }

    public function setMobile(?int $mobile): self
    {
        $this->mobile = $mobile;

        return $this;
    }

    public function getFacebook(): ?string
    {
        return $this->facebook;
    }

    public function setFacebook(?string $facebook): self
    {
        $this->facebook = $facebook;

        return $this;
    }

    public function getTwitter(): ?string
    {
        return $this->twitter;
    }

    public function setTwitter(?string $twitter): self
    {
        $this->twitter = $twitter;

        return $this;
    }

    public function getInstagram(): ?string
    {
        return $this->instagram;
    }

    public function setInstagram(?string $instagram): self
    {
        $this->instagram = $instagram;

        return $this;
    }

    public function getCommercialName(): ?string
    {
        return $this->commercial_name;
    }

    public function setCommercialName(string $commercial_name): self
    {
        $this->commercial_name = $commercial_name;

        return $this;
    }

    /**
     * @return Collection|NavLink[]
     */
    public function getNavLinks(): Collection
    {
        return $this->navLinks;
    }

    public function addNavLink(NavLink $navLink): self
    {
        if (!$this->navLinks->contains($navLink)) {
            $this->navLinks[] = $navLink;
            $navLink->setBusinessId($this);
        }

        return $this;
    }

    public function removeNavLink(NavLink $navLink): self
    {
        if ($this->navLinks->contains($navLink)) {
            $this->navLinks->removeElement($navLink);
            // set the owning side to null (unless already changed)
            if ($navLink->getBusinessId() === $this) {
                $navLink->setBusinessId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|BackgroundImage[]
     */
    public function getBackgroundImages(): Collection
    {
        return $this->backgroundImages;
    }

    public function addBackgroundImage(BackgroundImage $backgroundImage): self
    {
        if (!$this->backgroundImages->contains($backgroundImage)) {
            $this->backgroundImages[] = $backgroundImage;
            $backgroundImage->setBusinessId($this);
        }

        return $this;
    }

    public function removeBackgroundImage(BackgroundImage $backgroundImage): self
    {
        if ($this->backgroundImages->contains($backgroundImage)) {
            $this->backgroundImages->removeElement($backgroundImage);
            // set the owning side to null (unless already changed)
            if ($backgroundImage->getBusinessId() === $this) {
                $backgroundImage->setBusinessId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Description[]
     */
    public function getDescriptions(): Collection
    {
        return $this->descriptions;
    }

    public function addDescription(Description $description): self
    {
        if (!$this->descriptions->contains($description)) {
            $this->descriptions[] = $description;
            $description->setBusinessId($this);
        }

        return $this;
    }

    public function removeDescription(Description $description): self
    {
        if ($this->descriptions->contains($description)) {
            $this->descriptions->removeElement($description);
            // set the owning side to null (unless already changed)
            if ($description->getBusinessId() === $this) {
                $description->setBusinessId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Products[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Products $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setBusinessId($this);
        }

        return $this;
    }

    public function removeProduct(Products $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
            // set the owning side to null (unless already changed)
            if ($product->getBusinessId() === $this) {
                $product->setBusinessId(null);
            }
        }

        return $this;
    }
}
