<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NavLinkRepository")
 */
class NavLink
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Business", inversedBy="navLinks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $business_id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $label;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBusinessId(): ?Business
    {
        return $this->business_id;
    }

    public function setBusinessId(?Business $business_id): self
    {
        $this->business_id = $business_id;

        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }
}
